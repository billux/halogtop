#!/usr/bin/perl

#
# Dependencies: libterm-key-perl
#

use strict;
use warnings;
use threads;
use threads::shared;
use Term::ReadKey;
use Data::Dumper;
use Getopt::Long;
use Pod::Usage;


my $IPs  :shared = shared_clone({});
my $URLs :shared = shared_clone({});
my $opt  :shared = shared_clone({
    help => 0,
    mode => 'i',
    time => 2,
});

sub print_interactive_usage() {
    
    system('clear');
    print <<EOT
  Help screen of HAlogtop.

    q       quit the program
    h or ?  show this help screen
    u       switch to top URL list
    i       switch to top IP list
    any key refresh the list

  Press any key to go back to list.
EOT
;
    ReadKey(0);
}

sub print_list($$$) {

    my ($list, $maxItems, $maxLength) = @_;
    my $i = 0;

    foreach my $entry (sort {$list->{$b} <=> $list->{$a}} keys %{$list}) {
        print "$list->{$entry}\t".substr($entry, 0, $maxLength)."\n";
        $i++;
        last if ($i >= $maxItems);
    }
}

sub display_results($$$) {

    #
    # Display the result to stdout and refresh it each second.
    #

    my ($IPs, $URLs, $opt) = @_;
    my ($termWidth, $termHeight);
    my $mode = $opt->{'mode'};
    my $timeout = $opt->{'time'};
    my $keyPressed;
    my $quit = 0;

    ReadMode(4);
    END { ReadMode 0 }

    until ($quit) {
        
        ($termWidth, $termHeight) = GetTerminalSize;

        # First, clear screen.
        system('clear');

        print "Top list of IPs\n\n"  if ($mode eq 'i');
        print "Top list of URLs\n\n" if ($mode eq 'u');

        # Print only $termHeight-4 first entries of the list.
        print_list($IPs,  $termHeight-4, $termWidth-8) if ($mode eq 'i');
        print_list($URLs, $termHeight-4, $termWidth-8) if ($mode eq 'u');

        # Watches pressed keys to do some actions.
        # Wait 2 seconds for an entry. This allow to refresh screen each 2
        # seconds.
        $keyPressed = ReadKey($timeout);
        if (defined($keyPressed)) {
            print "$keyPressed\n";
            if ($keyPressed eq '?' || $keyPressed eq 'h') {
                print_interactive_usage();
            }
            elsif ($keyPressed eq 'q') {
                $quit = 1;
            }
            elsif ($keyPressed eq 'u') {
                $mode = 'u';
            }
            elsif ($keyPressed eq 'i') {
                $mode = 'i';
            }
        }
        
    }

    ReadMode(0);
    #threads->exit(); #if threads->can('exit');
    exit;
}


#
# Options parsing
#

GetOptions(
    'help|?' => \$opt->{'help'},
    'mode=s' => \$opt->{'mode'},
    'time=i' => \$opt->{'time'},
) or pod2usage(2);

pod2usage(1) if ($opt->{'help'});

pod2usage("Invalid value '$opt->{'mode'}' for mode option.")
    unless ($opt->{'mode'} eq 'i' || $opt->{'mode'} eq 'u');

pod2usage("Invalid value '$opt->{'time'}' for time option.")
    unless ($opt->{'time'} > 0);

pod2usage("Too many log files given.") if (@ARGV > 1);


#
# Start new thread to display results.
#

my $thread = threads->create(\&display_results, $IPs, $URLs, $opt);
$thread->detach();


#
# Parse log file and fill %IPs and %URLs hashes.
#

my @line;
my ($IP, $URL);
my $pipedOutput;

my $logFile = $ARGV[0] || '/var/log/haproxy.log';

open($pipedOutput, '-|', "tail -n1000 -F $logFile")
    or die $!;

while (<$pipedOutput>) {

    # An example of default HAProxy log line:
    # Apr  9 14:21:18 localhost hapee-lb[9305]: 172.16.14.8:33250 [09/Apr/2013:14:21:18.120] front_web web/www12 0/0/1/833/835 200 22823 - - ---- 218/194/202/36/0 0/0 "POST /service-xml/index_XML.php HTTP/1.1"
    @line = split ' ';

    # TODO/IDEA  Read current HAProxy log format from his config file.

    #print Dumper @line;

    # Remove source port.
    ($IP) = split ':', $line[5];

    # A line could be a bad request and could not contain a URL.
    if ($line[17] eq '"<BADREQ>"') {
        $URL = "BADREQ";
    }
    # A line could be a request from another reverse proxy, and so the source
    # IP is not correct and the real IP is in $line[17].
    elsif ($line[17] =~ /^\{(.+)\}$/) {
        $IP = $1;
        $URL = join(' ', $line[18], $line[19]);
    }
    else {
        $URL = join(' ', $line[17], $line[18]);
    }
    $URL =~ s/^"//;

    # Add IP to the hash, or increment it if it is already present.
    if (defined $IPs->{$IP}) {
        $IPs->{$IP}++;
    }
    else {
        $IPs->{$IP} = 1;
    }

    # Same for URL.
    if (defined $URLs->{$URL}) {
        $URLs->{$URL}++;
    }
    else {
        $URLs->{$URL} = 1;
    }
}
__END__

=head1 NAME

halogtop  -  interactive HAProxy log viewer

=head1 SYNOPSIS

halogtop [-h] [-t <time>] [-m <mode>] [<HAProxy's log file>]

  -h|--help         show help message
  -t|--time <time>  refresh interval of the screen (in seconds)
  -m|--mode <mode>  start in <mode> mode (can be changed at runtime)

<mode> is one of these:

  i     Top list of IPs
  u     Top list of URLs

<time> must be greater or egual than 1 (second).

=head1 DESCRIPTION

halogtop analyse HAProxy's log and display usefull information such as top list
of IPs or URLs. The aim is to have sames functionality as apachtop, but works
on HAProxy's log.
